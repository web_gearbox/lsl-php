<?php

function af_autoloader($name){
    include_once('classes'.DIRECTORY_SEPARATOR.$name.'.class.php');
}

spl_autoload_register('af_autoloader');