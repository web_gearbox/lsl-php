<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>KeyVal database generator</title>
</head>
<body>

<?php
include 'config.php';
include 'autoload.php';

$database_connection = new DBMySQLi($config['database']);
$keyval = new KeyVal($database_connection);

if($database_error = $database_connection->getError()){ ?>
    <h1>Database error.</h1>
    <p>Please, make sure the database is set correctly (make sure the <b>config.php</b> file has all the settings
        written in) and the database user  has sufficient permissions</p>
    <p>The error description was <i><?php echo($database_error);?></i></p><?php
}
else{
    echo('<p>Database connection successful.</p>');
    if(!empty($_REQUEST['createtable'])){

        $sql = 'CREATE TABLE IF NOT EXISTS `'.$keyval->table.'` (
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`key`)
) DEFAULT CHARSET=`utf8`;';
        $database_connection->query($sql);
        if($error = $database_connection->getError()){
            echo($error);
        }
    }
    $result = $database_connection->query('SHOW TABLES LIKE \''.$keyval->table.'\'')->fetch();
    if(empty($result)){?>
        <form action="install.php" method="get">
        <p>
            No table <b><?php echo $keyval->table;?></b> found in database.
            <input type="submit" name="createtable"  value="Create"/>
        </p>
        </form>
    <?php
    }
    else{
        echo('<p>The table found. All should be set now. Check by setting a
variable:<a href="index.php?method=set&key=installed&value=true">?method=set&key=installed&value=true</a></p>');
    }
}

?>

</body>
</html>