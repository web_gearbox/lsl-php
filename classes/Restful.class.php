<?php

class Restful{

    protected $response = array();

    final public function run($method=null,$arguments=array()){

        if(empty($method))$method='index';

        $this->respond('status','ok');

        $this->respond('method',$method);

        if($this->setup() && method_exists($this,$method)){
            $validation_result = $this->check($method,$arguments);
            if($validation_result === true) {
                call_user_func_array(array($this, $method), $arguments);
            }
            else{
                $this->error('validation_error',$validation_result);
            }
        }
        else{
            $this->error('method_undefined',$method);
        }

    }

    private function setup(){
        return true;
    }

    final public function error($error_code,$error_data=null){
        $this->respond('status','error');
        $this->respond('error_code',$error_code,true);
        if(!empty($error_data))
            $this->respond($error_code,$error_data,true);
    }

    function check($method,$arguments){
        return true;
    }

    final function __toString(){
        return $this->getResponseJSON();
    }

    final function respond($key,$value,$append=false){
        if($append)$this->response[$key][]=$value;
        else $this->response[$key] = $value;
    }

    final public function getResponseJSON(){
        return json_encode($this->response);;
    }

    final public function getResponseJSONP($function='jsonp'){
        return $function.'('.$this->getResponseJSON().');';
    }

}