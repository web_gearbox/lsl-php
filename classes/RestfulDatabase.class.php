<?php

class RestfulDatabase extends Restful{

    /** @var DBAbstract */
    public $database;

    public function __construct(DBAbstract $database){
        $this->setDatabase($database);
    }
    public function setDatabase(DBAbstract $database){
        $this->database = $database;
    }

    private function setup(){
        $database_error = $this->database->getError();
        if($database_error!==false){
            $this->error('database_error',$database_error);
            return false;
        }
        return true;
    }
}