<?php

class KeyVal extends RestfulDatabase{
    public $table = 'keyval';

    public function get($variable_name=null){
        $this->respond('key',$variable_name);
        $this->respond('value','test');
    }

    public function set($variable_name=null,$value=null){
        $this->respond('key',$variable_name);
        $this->respond('value',$value);
    }

    public function delete($variable_name){
        $sql = 'DELETE FROM '.$this->table['keyval'].' WHERE key=\''.$this->database->escape($variable_name).'\'';
        $this->database->query($sql)->fetch();
        $this->respond('key',$variable_name);
    }
}