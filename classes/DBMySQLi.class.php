<?php

class DBMySQLi implements DBAbstract
{

    /**
     * @var mysqli
     */
    private $connection;
    private $last_result;

    function __construct($parameters){
        $this->connection = $this->connect($parameters);
    }

    function query($query_text){
        $this->last_result = mysqli_query($this->connection, $query_text);
        return $this;
    }

    function fetch(){
        return mysqli_fetch_assoc($this->last_result);
    }

    function fetch_all(){
        $all = array();
        while($row = mysqli_fetch_assoc($this->last_result))
            $all[]=$row;
        return $all;
    }

    function getError(){
        if ($this->connection->connect_errno)
            return $this->connection->connect_error;
        else if ($this->connection->errno)
            return $this->connection->error;
        else return false;
    }

    function disconnect(){
        $this->connection->close();
        unset($this->connection);
    }

    function escape($string){
        return mysqli_real_escape_string($this->connection,$string);
    }

    function connect($parameters){
        return @(new mysqli(
            $parameters['host'],
            $parameters['username'],
            $parameters['password'],
            $parameters['dbname'],
            empty($parameters['port']) ? null : $parameters['port']
        ));
    }
}