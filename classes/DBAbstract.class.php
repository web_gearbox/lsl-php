<?php

interface DBAbstract{
    function getError();

    /**
     * @param $query_text
     * @return DBAbstract
     */
    function query($query_text);
    function fetch();
    function fetch_all();
    function connect($parameters);
    function disconnect();
    function escape($string);
}